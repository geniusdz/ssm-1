## 九、SSM整合
### 9.1创建web项目
* 创建maven项目
* 修改pom文件packing=war
* 完成maven工程web项目结果
* 添加web依赖
```xml
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
            <scope>provided</scope>
        </dependency>
```
* 配置服务器运行环境
### 9.2 部署MyBatis
* 添加依赖
```xml
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.19</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.4.6</version>
        </dependency>
```

### 9.3 部署Spring、SpringMVC
```xml
<dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
            <version>5.2.13.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.2.13.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.2.13.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>5.2.13.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.13.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.12.1</version>
        </dependency>
```

#### 9.3.2 创建spring配置（多配置文件分开配置）
> spring-context.xml  只配置注解声明、以及类的管理
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <!--声明使用注解配置-->
    <context:annotation-config/>
    <!--声明Spring工厂注解的扫描范围-->
    <context:component-scan base-package="com.qfedu"/>


</beans>
```
> spring-mvc.xml  进行mvc相关配置，例如静态资源、拦截器配置等
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <mvc:annotation-driven/>



</beans>
```
> spring-mybatis.xml 进行spring与mybatis整合的相关配置
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/aop
        http://www.springframework.org/schema/aop/spring-aop.xsd">

    <context:annotation-config/>
    <context:component-scan base-package="com.qfedu"/>
    


</beans>
```
#### 9.3.3 配置SpringMVC前端控制器
* web.xml中配置
```xml
<servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:spring-*.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
```

### 9.4 整合配置(IOC)
#### 9.4.1 导入mybatis-spring依赖
```xml
<dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.2</version>
        </dependency>
```

#### 9.4.2 配置druid
* 添加druid依赖
```xml
<dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.1.10</version>
        </dependency>
```
* 创建druid.properties属性文件
```
druid.driver=com.mysql.cj.jdbc.Driver
druid.url=jdbc:mysql://localhost:3306/blog?characterEncoding=utf-8
druid.username=root
druid.password=root

## 连接池配置
druid.pool.init=1
druid.pool.minIdle=3
druid.pool.maxActive=20
druid.pool.timeout=30000
```
* 配置spring-mybatis.xml
```xml
<context:property-placeholder location="druid.properties"/>


    <bean id="druidDataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <property name="driverClassName" value="${druid.driver}"/>
        <property name="url" value="${druid.url}"/>
        <property name="username" value="${druid.username}"/>
        <property name="password" value="${druid.password}"/>

        <property name="initialSize" value="${druid.pool.init}"/>
        <property name="minIdle" value="${druid.pool.minIdle}"/>
        <property name="maxActive" value="${druid.pool.maxActive}"/>
        <property name="maxWait" value="${druid.pool.timeout}"/>
    </bean>
```
#### 9.4.3 配置sqlSessionFactory
* 在spring-mybatis.xml配置数据源
```xml
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="druidDataSource"/>
        <property name="mapperLocations" value="classpath:mappers/*.xml"/>
        <property name="typeAliasesPackage" value="com.qfedu.bean"/>
        <property name="configLocation" value="classpath:mybatis-config.xml"
    </bean>
```
#### 9.4.4 配置MapperScannerConfigurer
```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/> 
        <property name="basePackage" value="com.qfedu.dao"/>
    </bean>
```

### 9.5 整合配置（AOP）
> 使用Spring提供的事务管理完成DAO操作的事务管理
> 
>基于注解的事务管理配置：
* 将Spring提供的事务管理切面类配置到Spring容器
```xml
<!--AOP配置-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="druidDataSource"/>
    </bean>
    
    <tx:annotation-driven transaction-manager="transactionManager"/>
```
### 9.6 整合测试
#### 9.6.1 完成User查询操作
* 创建实体类
```java
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private int userId;
    private String userName;
    private String userPwd;
    private String userRealname;
    private String userImg;
}
```
* 在DAO包中创建接口
* 在Mappers下创建映射文件
```xml
<resultMap id="userMap" type="User">
        <id column="user_id" property="userId"/>
        <result column="user_name" property="userName"/>
        <result column="user_pwd" property="userPwd"/>
        <result column="user_realname" property="userRealname"/>
        <result column="user_img" property="userImg"/>
    </resultMap>

    <select id="queryUserByName" resultType="userMap">
        select user_id,user_name,user_pwd,user_realname,user_img
        from users
        where user_name=#{userName}
    </select>
```

#### 9.6.2 对DAO单元测试
* 添加依赖
```xml
<dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
```
* 创建测试类
```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context.xml","classpath:spring-mvc.xml","classpath:spring-mybatis.xml"})
public class UserDAOTest {

    @Resource
    private UserDAO userDAO;

    @org.junit.Test
    public void queryUserByName() {
        User user=userDAO.queryUserByName("ganyu");
        System.out.println(user);
    }
}
```
> 坑：Mapper中resultMap 以及数据库连接设置时区?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf-8&useSSL=false
> 都2121年了为什么还要设置时区 -_- 无语~~~
```html
<base href="${pageContext.request.contextPath}/">
设置浏览器的url
```

## 一、VUE
## 二、MVVM
### 2.1VM的实现原理
> view model中内置了一个观察者，这个观察者观察两个维度
> 1）观察试图的变化：当试图改变通知数据变化
> 2）观察数据变化
> ----MVVM通过VM实现了 `双向数据绑定`
## 三、vue快速开始
### 3.1 导入
```html
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
```
### 3.2 如何在页面使用vue
> 两个部分
* html：
```html
<div id="app"></div>
```
* js:vue对象
### 3.3 vue对象
```html
new Vue({
        el:'#app',//element,该vue对象绑定在哪个div
        data:{//提供数据
            title:'hello vue',//以后数据通过发送ajax请求获得
            name:'甘雨',
            age:2000
        }
    });//json格式对象
```
### 3.4 在html的被vue绑定的元素中，通过插值表达式来获取vue对象的数据
```html
<div id="app">
    <span>
        {{title}}
    </span>
    <br>
    <input type="text" v-model="title"/>
    <br>
    欢迎 {{age}}&{{name}}
</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>

    new Vue({
        el:'#app',//element
        data:{
            title:'hello vue',//以后数据通过发送ajax请求获得
            name:'甘雨',
            age:2000
        }
    });//json格式对象
```

## 四、插值表达式
* 插值表达式是用在html中被绑定的元素中的。目的是通过插值表达式来获取vue对象中的属性和方法。
```html
<div id="app">
    我是{{name}}，{{[1,2,3,4][2]}}<br>
    {{{"name":"ganyu","age":"2000"}.age}}
    {{sayHi()}}
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>

    new Vue({
        el:'#app',
        data:{
            title:'hello vue',
            name:'甘雨',
            age:2000
        },
        methods:{
            sayHi:function(){
                alert("hello vue");
            }
        }
    });
```
## 五、vue中的关键字
### 5.1 v-model
> 是将标签的value值与vue实例中的data属性值进行绑定
### 5.2 v-on
> v-on叫绑定事件
```html
<input type="text" v-on:input="changeMajor()"/>
```
* 补充：比如在响应函数里，可以指明使用event内置的参数对象。该对象表示当前事件，可以通过event.target.value来获得当前事件对象的value的值。
```html
changeMajor:function(event){
                this.major=event.target.value;
            }
```
* 简写：可以使用@替换v-on：
```html
<input type="text" @input="changeMajor()"/>
```
### 5.3 v-bind
```html
<a v-bind:href="link">baidu</a>
可以缩写成 ：
<a :href="link">baidu</a>
```
### 5.4 v-html和v-once
> v-once指明此元素的数据只出现一次，数据内容的修改不影响此元素
* v-html会将vue中的属性的值作为html元素来使用
* v-text会将vue中的属性的值只作为纯文本来使用
## 六、事件
### 6.1 vue中如何使用事件
### 6.2 事件的参数传递
* 设参
```html
<button type="button" @click="addbtnfn(2)">add</button>
```
* 传参
```html
addbtnfn:function(step){
```
* 接参
```html
this.count+=step;
```
### 6.3 vue中的事件修饰符
* @click.stop 阻止点击事件的传播
* @mouseover.stop 阻止鼠标移动事件
* @keyup.space 空格键弹起时
## 七、vue改变内容--虚拟DOM和diff算法
### 7.1 插值表达式的方式
```html
<div id="app">
    {{count}}
    {{result}}
    <button type="button" @click="addbtnfn(2)">add</button>
</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    new Vue({
        el:'#app',
        data:{
           count:0,
            result:''
        },
        methods:{
            addbtnfn:function(step){
                this.count+=step;
                this.result=this.count>10?'大于10':'不大于10';
            }
        }
    })
```
### 7.2 计算属性computed
> 计算属性的重点在属性两个字上，他是个属性其次有计算的能力，他就是将计算结果缓存起来的属性（将行为转化为了静态的属性）
* 计算属性与方法的区别
```html

    {{getCurrentTime()}}
    {{getCurrentTime1}}


        methods:{
            addbtnfn:function(step){
                this.count+=step;
                this.result=this.count>10?'大于10':'不大于10';
            },
            getCurrentTime:function(){
                return new Date();
            },

        },
        computed:{
            getCurrentTime1:function(){
                return new Date();
            }
        }
```
* watch监控属性:通过watch给属性绑定函数，当函数值改变时自动调用，调用时可以接收newVale,oldValue两个参数
```html
    
<input type="text" v-model="title"/>


        watch:{
            title:function (newVale,oldValue) {
                console.log(newVale+':'+oldValue);
            }
        }
```
## 八、vue改变样式
### 8.1 class的动态绑定
* 通过给html元素的class属性绑定vue中的属性值，得到样式的动态绑定
```html
<div class="mydiv"></div>

    <button type="button" @click="temp=!temp">temp</button>
</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    new Vue({
        el:'#app',
        data:{
            temp:false
        }
    })
```
### 8.2 加入computed
```html
<div :class="myClassStyle" class="mydiv"></div>

computed:{
            myClassStyle:function () {
                return{
                    green:this.temp,
                    mywidth:this.temp
                }
            }
        }
```
### 8.3 双向绑定的体现
```html
<div class="mydiv" :class="color"></div>

data:{
            temp:false,
            temp1:false,
            color:"green"
        },
```
### 8.4 多个样式操作
* 使用数组绑定多个样式
```html
<div class="mydiv" :class="[color,mywidth]"></div>
```
### 8.5 通过style设置样式
```html
<div class="mydiv" :style="{backgroundColor:color}"></div>
```
### 8.6 使用computed设置样式
```html
<div id="app">
    <input type="text" v-model="width"/>
    <div class="mydiv" :style="myStyle"></div>

</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    new Vue({
        el:'#app',
        data:{
            width:100,
            color:"green"
        },
        computed:{
            myStyle:function () {
                return{
                    backgroundColor:this.color,
                    width:this.width+"px"
                }
            }
        }
    })
```
### 8.7 设置style属性的多个样式
* 在style中使用多个样式的组合（数组），其中json对象键值对使用{}包裹
```html
<div class="mydiv" :style="[myStyle,{height:myHeight+'px'}]"></div> 
```

## 九、Vue核心：虚拟DOM和diff算法
* vue的高效的核心，就是虚拟DOM和diff算法，vue不通过修改dom树来达到修改的效果，而是直接在页面上改那个元素，此时这个元素就是一个虚拟的DOM。
那么vue怎么去改呢？通过diff算法，计算出虚拟的DOM修改后与修改前的区别，然后在虚拟DOM的原基础上进行修改，这样效率就大大提升了。
## 十、vue中的语句
### 10.1 v-if,v-else-if,v-else
```html
<div id="app">
    <div v-if="temp">if</div>
    <div v-else-if="temp1">if-else-if</div>
    <div v-else="temp">else</div>

    <button type="button" @click="temp=!temp">temp</button>
    <button type="button" @click="temp1=!temp1">temp1</button>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    new Vue({
        el:"#app",
        data:{
            temp:false,
            temp1:false
        }
    })
```
### 10.2 v-show
```html
<div v-show="temp">v-show</div>
```
* v-show用法上和v-if一样，但是v-show改变的是元素样式display，而v-if是直接让元素消失和添加元素，效率上v-show更高。
### 10.3 template模板标签的使用
```html
<template v-if="temp">
        <div>dd</div>
        <p>ifdd</p>
    </template>
```
* 该标签配合v-if实现多个元素一起出现或消失
### 10.4 v-for
* v-for通过定义的数据源遍历数据
```html
    <ul>
        <li v-for="a in args">{{a}}</li>
    </ul>

        data:{
            args:[1,2,3,4,5,6]
        }
```
* 带索引的for
```html
    <ul>
        <li v-for="(a,i) in args" :key="i">{{i}}--{{a}}</li>
    </ul>
```
* 遍历对象数组：嵌套的for循环
```html
<ul>
        <li v-for="student in students">
            <span v-for="(v,k,i) in student">{{i}}-{{k}}--{{v}}</span>
        </li>
    </ul>

        data:{
            students: [
               {name:"ganyu",age:2000},
                {name:"hutao",age:200}
            ]
        }
```

## 十一、vue实例（对象）
### 11.1 vue的实例属性
> vue中的el，data等等这些键是vue对象的实例属性
* 注意：ref的使用
> 在vue中，往往使用ref属性来代替id属性来使用。
```html
<button type="button" ref="mybtn1" @click="showVueObject">show</button>


        methods:{
            showVueObject:function () {
                this.$refs.mybtn1.innerHTML="hello";
            }
        }
```
### 11.2 mount的使用
* 实现页面元素和vue对象的动态绑定，之前通过el的方式绑定，也可以通过mount实例属性进行绑定。
```html
v1.$mount("#app")
```

## 十二、vue组件
* vue的一大特性：组件化。可以将vue对象作为一个组件，被反复使用。
要想实现组件化，需要在页面注册组件；关于注册方式有两种：全局注册，本地注册。
### 12.1 全局注册组件（component）
* 在被vue绑定的html元素中使用组件。
```html
<div id="app">
    <model1></model1>
    <model1></model1>

</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    Vue.component("model1",{
       template:"<div>{{title}}<button type='button' ref='mybtn1' @click='btnfn'>show</button></div>",
        data:function () {
            return{
                title:"hello"
            }
        },
        methods:{
           btnfn:function () {
               this.$refs.mybtn1.innerHTML="hello";
           }
        }
    });
    
    var v1=new Vue({
        el:"#app",
    });
```
* 注意：component与new vue的区别： template属性中有且只能有一个根标签；data写法是函数。

### 12.2 vue组件本地注册
* components属性本地绑定vue的div才能使用，其他被vue绑定的div不能使用该组件。
```html
<div id="app">
    <model1></model1>
    <model1></model1>

</div>

</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script>
    var model1={
        template:"<div>{{title}}<button type='button' ref='mybtn1' @click='btnfn'>show</button></div>",
        data:function () {
            return{
                title:"hello"
            }
        },
        methods:{
            btnfn:function () {
                this.$refs.mybtn1.innerHTML="hello";
            }
        }
    }

    var v1=new Vue({
        el:"#app",
        components:{
            "model1":model1
        }
    });
```
## 十三、vue-cli脚手架工具
### 13.1 安装node.js
### 13.2 安装vue-cli
```html
npm install vue-cli -g
```
